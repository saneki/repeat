#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// Size of buffer to use when reading/writing "chunks"
#define BUFFER_SIZE (4096)

// Overflow macro for size_t
#define OVERFLOW_SIZE(A, B) ((A) > SIZE_MAX - (B))

/**
 Output the concatenation of argv's elements N times.
 **/
int with_args(int argc, const char *argv[], long count)
{
	for (long c = 0; c < count; c++)
		for (int i = 0; i < argc; i++)
			printf("%s", argv[i]);
	return EXIT_SUCCESS;
}

/**
 Read all of stdin into a buffer and output N times.
 **/
int without_seek(long count)
{
	uint8_t *buffer, *temp;
	size_t length, num, offset;

	length = BUFFER_SIZE;
	buffer = (uint8_t *)malloc(length);
	if (!buffer) {
		perror("malloc");
		return EXIT_FAILURE;
	}
	offset = 0;

	do {
		// Read chunk from stdin
		num = fread(buffer + offset, 1, BUFFER_SIZE, stdin);
		if (OVERFLOW_SIZE(length, BUFFER_SIZE)) {
			fprintf(stderr, "error: overflow\n");
			free(buffer);
			return EXIT_FAILURE;
		}
		// Reallocate buffer with new length
		temp = (uint8_t *)realloc(buffer, length + BUFFER_SIZE);
		// Make sure reallocation succeeded
		if (!temp) {
			perror("realloc");
			free(buffer);
			return EXIT_FAILURE;
		}
		buffer = temp;
		length += num;
		offset += num;
	} while (num == BUFFER_SIZE);

	// Write N times to stdout
	for (long i = 0; i < count; i++) {
		fwrite(buffer, 1, offset, stdout);
	}

	free(buffer);
	return EXIT_SUCCESS;
}

/**
 Read from stdin and output N times while seeking. Can only be used on stdin
 streams that support seeking.
 **/
int with_seek(long count)
{
	uint8_t buffer[BUFFER_SIZE];
	size_t num;

	for (long i = 0; i < count; i++) {
		// Seek to beginning
		if (fseek(stdin, 0, SEEK_SET)) {
			perror("fseek");
			return EXIT_FAILURE;
		}
		// Read and write from stdin -> stdout
		do {
			num = fread(buffer, 1, sizeof(buffer), stdin);
			num = fwrite(buffer, 1, num, stdout);
		} while (num == sizeof(buffer));
	}

	return EXIT_SUCCESS;
}

int main(int argc, const char *argv[])
{
	const char *buffer;
	long count;
	int result;

	if (argc <= 0)
		exit(EXIT_SUCCESS);
	else if (argc == 1) {
		fprintf(stderr, "usage: %s N [text...]\n", argv[0]);
		exit(EXIT_SUCCESS);
	}

	count = strtol(argv[1], NULL, 0);

	if (argc >= 3)
		result = with_args(argc - 2, argv + 2, count);
	else {
		if (fseek(stdin, 0, SEEK_SET))
			result = without_seek(count);
		else
			result = with_seek(count);
	}

	return result;
}
